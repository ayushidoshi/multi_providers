
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Quests.dart';
import 'Rows.dart';

class QuestListView extends StatelessWidget{
  List<String> questList;

  @override
  Widget build(BuildContext context) {
    this.questList=Quests.instance.functionToGetList();
    return Container(
      height: 1100,
      color: Colors.white,
      child: ListView.builder(
          padding: EdgeInsets.zero,
          itemCount: questList.length,
          itemBuilder: (BuildContext context, int position) {
            debugPrint('ayushi here');
            return Consumer<Rows> (
                builder: (_, provider, __) => Container(
              margin: EdgeInsets.all(20),
              child: Text( "${provider.questList[position]}",style: TextStyle(fontSize: 30),),
            )
            );
          })
    );
  }
}
//here is the main code for