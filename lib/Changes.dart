
import 'dart:convert';

import 'package:flutter/cupertino.dart';

import 'Quests.dart';

class Changes extends ChangeNotifier{
  delete(String value){
    if(value!="") {
      (Quests.instance.quests).remove(value);
      notifyListeners();
    }
  }
}