import 'package:flutter/cupertino.dart';

import 'Quests.dart';

class Rows extends ChangeNotifier{
  List<String> questList=Quests.instance.quests;

  changeRow(int index,String val){
    Quests.instance.quests[index]=val;
    notifyListeners();
  }
}