
import 'Changes.dart';
import 'Rows.dart';

class Quests{
  static Quests _instance;
  List<String> quests=["Q1","Q2","Q3","Q4","Q5","Q6","Q7","Q8","Q9","Q10","Q11","Q12","Q13","Q14","Q15","Q16","Q17","Q18","Q19","Q20","Q21"];
  Quests._();

  static Quests get instance {
    if (_instance == null) {
      _instance = Quests._();
    }
    return _instance;
  }

  functionToGetList(){
    print("ayushi doshi quests" + quests.toString());
    return quests;
  }

  removeIt(Changes change,String val){
    print("ayushi string "+ val.toString());
    change.delete(val);
  }

  modifyIt(Rows modify, int index,String val){
    modify.changeRow(index, val);
  }
}