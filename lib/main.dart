import 'package:flutter/material.dart';
import 'package:multi_providers/QuestsListView.dart';
import 'package:provider/provider.dart';

import 'Changes.dart';
import 'Quests.dart';
import 'Rows.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MultiProvider(  // Multi means you can have more providers if you need
      providers: [
      ChangeNotifierProvider<Rows>(create: (context) => Rows()),
      ChangeNotifierProvider<Changes>(create: (context) => Changes()),
    ],
    child:MyHomePage()//ChangeNotifierProvider<Changes>(create: (context) => Changes(),child: MyHomePage()),
    ));
  }
}


class MyHomePage extends StatelessWidget {
  String heading="Q5";
  TextEditingController _controller;

  @override
  void initState() {
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final change= Provider.of<Changes>(context, listen: false);
    final rows = Provider.of<Rows>(context, listen: false);
    return Material(
      child: Column(
        children: [
          Text("delete"),
          TextField(
            controller: _controller,
            onChanged: (value) => heading = value,
          ),
          // Text("change"),
          // TextField(),
          RaisedButton(
              onPressed: (){
                if(heading.length>=3 && heading[0]=="D")
                Quests.instance.removeIt(change, heading.substring(1));
                else if(heading.length>=4 && heading[0]=="M") {
                  int index = 1,val=0;
                  while (heading[index] != "Q") {
                    val=val*10+int.parse(heading[index]);
                    index++;
                  }
                  Quests.instance.modifyIt(
                      rows, val, heading.substring(index));
                }
              }
          ),
          Consumer<Changes>(builder: (_,change,__)=>
            QuestListView(),
          )
        ],
      )
    );
  }
}
